from datetime import datetime

import pytz
from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=20,
                            verbose_name='Тэг рассылки',
                            blank=True, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тэг рассылки'
        verbose_name_plural = 'Тэги рассылки'


class MobileCode(models.Model):
    code = models.CharField(max_length=3, blank=False, unique=True)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ['code']
        verbose_name = 'Код оператора'
        verbose_name_plural = 'Коды оператора'


class Mailing(models.Model):
    description = models.CharField(max_length=30, blank=True, default='')
    start = models.DateTimeField(verbose_name='Начало')
    stop = models.DateTimeField(verbose_name='Окончание')
    message = models.TextField(verbose_name='Текст сообщения', blank=False)
    filter_tags = models.ManyToManyField(Tag)
    filter_mobile_code = models.ManyToManyField(MobileCode)

    @property
    def remained(self):
        """ Сколько сообщений в очереди на отправку """
        return self.messages.filter(sent_time_utc__isnull=True).count()

    @property
    def messages_count(self):
        """ Сколько сообщений в рассылке """
        return self.messages.count()

    def __str__(self):
        return self.description

    class Meta:
        ordering = ['-id']
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    short_phone = models.CharField(max_length=10,
                                   verbose_name='Короткая часть номера')
    mobile_code = models.ForeignKey(MobileCode,
                                    on_delete=models.CASCADE,
                                    related_name='clients')
    tag = models.ForeignKey(Tag, on_delete=models.DO_NOTHING,
                            blank=True, null=True, related_name='clients')

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    time_zone = models.CharField(max_length=32,
                                 choices=TIMEZONES,
                                 default='UTC')
    created = models.DateTimeField(auto_now=True,
                                   verbose_name='Дата создания клиента')

    @property
    def phone(self):
        """ Вывод полного номера """
        return f'7{self.mobile_code.code}{self.short_phone}'

    @property
    def client_time(self):
        """ Локальное время клиента """
        return datetime.now(pytz.timezone(self.time_zone))

    def __str__(self):
        return self.phone

    class Meta:
        ordering = ['-created']
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        unique_together = ['mobile_code', 'short_phone']


class Message(models.Model):
    sent_time_utc = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now=True,
                                   verbose_name='Поставлено в очередь')

    class StatusChoices(models.IntegerChoices):
        CREATED = 0, 'Created'
        SEND_WAIT = 1, 'Wait'
        SEND_OK = 2, 'Sent'
        ERROR = 3, 'Error'
        WRONG_TIME = 4, 'Overdue'

    status_send = models.PositiveSmallIntegerField(
        choices=StatusChoices.choices,
        default=StatusChoices.CREATED)

    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE,
                                related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.DO_NOTHING,
                               related_name='messages')
    remain_tries = models.IntegerField(
        default=5,
        verbose_name='Осталось попыток отправки')

    def __str__(self):
        return self.mailing.description + ' ' + self.client.phone

    class Meta:
        ordering = ['-created']
        verbose_name = 'Сообщения'
        verbose_name_plural = verbose_name
        unique_together = ['mailing', 'client']
