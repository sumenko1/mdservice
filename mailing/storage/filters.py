import django_filters as df

from storage.models import Client


class ClientFilter(df.FilterSet):
    tag = df.CharFilter(field_name='tag__name', method='tag_filter')
    code = df.CharFilter(field_name='mobile_code__code', method='code_filter')

    class Meta:
        model = Client
        fields = ('tag', 'code')

    def tag_filter(self, queryset, name, value):
        tags = self.request.query_params.getlist('tag')
        return queryset.filter(tag__name__in=tags)

    def code_filter(self, queryset, name, value):
        codes = self.request.query_params.getlist('code')
        return queryset.filter(mobile_code__code__in=codes)
