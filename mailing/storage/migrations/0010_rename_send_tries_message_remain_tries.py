# Generated by Django 3.2.7 on 2022-04-08 14:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0009_message_send_tries'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='send_tries',
            new_name='remain_tries',
        ),
    ]
