from django.contrib import admin

from .models import Client, Mailing, Message, Tag


class AdminMailing(admin.ModelAdmin):
    list_display = ('id', 'description', 'start', 'stop')


class AdminClient(admin.ModelAdmin):
    list_display = ('phone', 'mobile_code', 'tag', 'time_zone')


class AdminMessages(admin.ModelAdmin):
    list_display = ('id', 'mailing', 'client', 'status_send', 'message',
                    'created', 'remain_tries')

    def message(self, instance):
        return instance.mailing.message


class AdminTags(admin.ModelAdmin):
    pass


admin.site.register(Tag, AdminTags)
admin.site.register(Mailing, AdminMailing)
admin.site.register(Client, AdminClient)
admin.site.register(Message, AdminMessages)
