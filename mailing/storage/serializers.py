import re

import pytz
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.forms import ValidationError
from rest_framework import serializers

from .models import Client, Mailing, Message, MobileCode, Tag


class ClientSerializer(serializers.ModelSerializer):
    tag = serializers.CharField(label='Тэг', required=False)
    phone = serializers.CharField(label='Номер телефона')
    mobile_code = serializers.StringRelatedField(label='Код оператора',
                                                 required=False)

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    time_zone = serializers.ChoiceField(choices=TIMEZONES)

    class Meta:
        model = Client
        fields = ['id', 'phone', 'tag', 'mobile_code', 'time_zone']

    def validate_phone(self, value):
        if not re.fullmatch(r'7\d{10}', value):
            raise serializers.ValidationError(
                'Неверный номер. Формат номера должен быть  7XXXXXXXXXX')
        short_phone = value[4:]
        mobile_code = value[1:4]
        context = self.context
        method = context.get('request').method
        if method != 'POST':
            obj_id = int(context.get('request').parser_context['kwargs']['pk'])
        # проверяем что клиент есть, получаем объект или none
        obj = Client.objects.filter(short_phone=short_phone,
                                    mobile_code__code=mobile_code)

        if ((obj.exists() and method == 'POST')
            or (obj.exists() and method != 'POST'
           and obj[0].id != obj_id)):
            raise serializers.ValidationError(
                'Номер существует')
        return value

    @transaction.atomic
    def create(self, validated_data, client=None, update=False):
        phone = validated_data.pop('phone')
        time_zone = validated_data.pop('time_zone')
        code = phone[1:4]
        short_phone = phone[4:]
        code_obj, _ = MobileCode.objects.get_or_create(code=code)
        tag = validated_data.get('tag')

        if not client:
            client = (Client.objects.
                      create(short_phone=short_phone,
                             mobile_code=code_obj, time_zone=time_zone,
                             tag=None))
        else:
            client.short_phone = short_phone
            client.mobile_code = code_obj
            client.time_zone = time_zone
        if not tag:
            tag = ''

        client.tag, _ = Tag.objects.get_or_create(name=tag)
        client.save()
        return client

    def update(self, instance, validated_data):
        return self.create(validated_data=validated_data, client=instance)


class TagSerializer(serializers.StringRelatedField):
    def to_internal_value(self, data):
        try:
            return Tag.objects.get(name=data)
        except ObjectDoesNotExist:
            raise ValidationError(f'{data} - тег не существует')


class MobileCodeSerializer(serializers.StringRelatedField):
    def to_internal_value(self, data):
        try:
            return MobileCode.objects.get(code=data)
        except ObjectDoesNotExist:
            raise ValidationError(f'{data} - код не существует')


class MessageSerializer(serializers.ModelSerializer):
    message = serializers.CharField(source='mailing.message')
    status_send = serializers.SerializerMethodField()

    def get_status_send(self, obj):
        """ get_FOO_display() для chice полей """
        return obj.get_status_send_display()

    class Meta:
        model = Message
        fields = ('id', 'client', 'mailing', 'message', 'status_send')


class MailingSerializer(serializers.ModelSerializer):
    filter_tags = TagSerializer(many=True)
    filter_mobile_code = MobileCodeSerializer(many=True)

    def validate(self, attrs):
        if attrs['stop'] <= attrs['start']:
            raise ValidationError('Неверная дата начала и окончания рассылки')
        return super().validate(attrs)

    class Meta:
        model = Mailing
        fields = ['id', 'description', 'start', 'stop',
                  'message', 'filter_tags', 'filter_mobile_code',
                  'remained', 'messages_count']

    def save_data(self, data, obj=None):
        filter_tags = data.pop('filter_tags')
        filter_mobile_code = data.pop('filter_mobile_code')

        if not obj:
            obj = Mailing.objects.create(
                description=data.pop('description'),
                start=data.pop('start'),
                stop=data.pop('stop'),
                message=data.pop('message'),
            )

        obj.filter_tags.set(filter_tags)
        obj.filter_mobile_code.set(filter_mobile_code)
        obj.save()
        return obj

    @transaction.atomic
    def create(self, validated_data):
        return self.save_data(data=validated_data)

    @transaction.atomic
    def update(self, instance, validated_data):
        return self.save_data(data=validated_data, obj=instance)
