from django.db.models import fields
from django.test import TestCase
from storage.models import Client, MobileCode, Tag


class TestClientModel(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.code = MobileCode.objects.create(code='926')
        cls.tag = Tag.objects.create(name='men')

        cls.client = Client.objects.create(short_phone='3760097',
                                           mobile_code=cls.code,
                                           tag=cls.tag,
                                           time_zone='Europe/Moscow')

    def test_phone_property(self):
        """ Проверка работы property phone в Client """
        self.assertEqual(str(TestClientModel.client), '79263760097',
                         "Client.__str__ должно возвращат номер "
                         "в формате 7XXXXXXXXXX")

        self.assertEqual(TestClientModel.client.phone, '79263760097',
                         'property phone должно возвращат номер '
                         'в формате 7XXXXXXXXXX')

    def test_fields(self):
        """ Проверка полей модели клиента """
        client = TestClientModel.client

        tests = [('short_phone', fields.CharField),
                 ('mobile_code', fields.related.ForeignKey),
                 ('tag', fields.related.ForeignKey),
                 ('time_zone', fields.CharField)]

        for field_name, field_type in tests:
            with self.subTest(value=field_name):
                test_type = type(client._meta.get_field(field_name))
                self.assertEqual(test_type, field_type,
                                 f'Поле {field_name} должно быть {field_type}')
