from datetime import datetime, timedelta

import pytz
from django.shortcuts import get_list_or_404
from django.test import TestCase
from sender.sender import send_messages
from storage.models import Client, Mailing, Message, MobileCode, Tag


class TestClientModel(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.clients = [
            {'code': '926', 'phone': '3760097',
             'tz': 'Europe/Moscow', 'tag': 'rus'},
            {'code': '914', 'phone': '1230011',
             'tz': 'Europe/London', 'tag': 'gbr'},
            {'code': '123', 'phone': '3332200',
             'tz': 'US/Michigan', 'tag': 'usa'},
            {'code': '999', 'phone': '1112200',
             'tz': 'Australia/Melbourne', 'tag': 'aus'},
            {'code': '222', 'phone': '0009911',
             'tz': 'Australia/Melbourne', 'tag': 'aus'},
        ]
        for client in cls.clients:
            cls.code = MobileCode.objects.create(code=client['code'])
            cls.tag, _ = Tag.objects.get_or_create(name=client['tag'])

            cls.client = Client.objects.create(
                short_phone=client['phone'],
                mobile_code=cls.code,
                tag=cls.tag, time_zone=client['tz'])
        start = datetime.now(pytz.timezone('UTC'))
        stop = start + timedelta(hours=2)
        cls.mailing = Mailing.objects.create(description='test', start=start,
                                             stop=stop,
                                             message='test message')
        tags = get_list_or_404(Tag,
                               name__in=[c['tag'] for c in cls.clients[:3]])

        cls.mailing.filter_tags.set(tags)

        codes = get_list_or_404(MobileCode,
                                code__in=[c['code'] for c in cls.clients])
        cls.mailing.filter_mobile_code.set(codes)
        cls.mailing.save()

    def test_mailing_timezone(self):
        """
        Сообщения должны отправляться в нужное для таймзоны время
        """
        send_messages(debug=True)
        codes = [Message.StatusChoices.WRONG_TIME,
                 Message.StatusChoices.SEND_OK,
                 Message.StatusChoices.SEND_WAIT]
        for n, message in enumerate(
                Message.objects.order_by('-status_send').all()):
            with self.subTest(value=message.client.phone):
                self.assertEqual(message.status_send, codes[n],
                                 ('У сообщения на номер {} неверный статус {} '
                                  'отправки для часового пояса {} должно'
                                  ' быть {}'.
                                  format(message.client,
                                         Message.StatusChoices.
                                         choices[message.status_send][1],
                                         message.client.time_zone,
                                         Message.
                                         StatusChoices.choices[codes[n]][1])))

    def test_mailing_filters_tag(self):
        """ Сообщения должны фильтроваться по тэгу """

        messages_count_first = Message.objects.all().count()
        self.assertEqual(messages_count_first, 3,
                         'неверное исходное количество сообщений в очереди')

    def test_mailing_filter_code(self):
        """ Сообщения должны фильтроваться по коду оператора """

        messages_count_first = Message.objects.all().count()

        start = datetime.now(pytz.timezone('UTC'))
        stop = start + timedelta(hours=2)

        mailing = Mailing.objects.create(description='test', start=start,
                                         stop=stop,
                                         message='test message')
        mailing.filter_tags.set(get_list_or_404(Tag,
                                                name__in=['msk',
                                                          'aus',
                                                          'gbr']))

        mailing.filter_mobile_code.set(get_list_or_404(
            MobileCode,
            code__in=['222', '999']))
        mailing.save()

        messages_count_second = Message.objects.all().count()
        inc = messages_count_second - messages_count_first
        self.assertEqual(inc, 2,
                         'Неверное количество сообщений по фильтру'
                         ' код оператора')
