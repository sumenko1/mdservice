from django.test import Client as TestClient
from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse
from storage.models import Client, MobileCode, Tag


class TestClientModel(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.code = MobileCode.objects.create(code='926')
        cls.tag = Tag.objects.create(name='men')

        cls.client = Client.objects.create(short_phone='3760097',
                                           mobile_code=cls.code,
                                           tag=cls.tag,
                                           time_zone='Europe/Moscow')

    def setUp(self):
        self.test_client = TestClient()
        self.api_prefix = '/api/v1/'

    def test_views_list_endpoints(self):
        """ Проверяем наличие основных view функций"""
        ns = 'storage'
        views = ['clients', 'mailings', 'messages']

        for v in views:
            with self.subTest(name=v, msg=f'Test view function {v}'):
                resp = self.test_client.get(reverse(f'{ns}:{v}-list'))
                self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_basic_endpoints(self):
        """ Проверяем основные эндпоинты на отзывчивость """
        endpoints = [
            'clients', 'mailings', 'messages'
        ]
        for ep in endpoints:
            with self.subTest(value=ep):
                resp = self.test_client.get(f'{self.api_prefix}{ep}/')
                self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_add_new_client_ok(self):
        count_one = Client.objects.count()
        data = {
            'phone': '79140001223',
            'tag': 'man',
            'time_zone': 'Europe/Moscow'
        }
        resp = self.test_client.post(self.api_prefix + 'clients/', data=data)
        count_two = Client.objects.count()
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED,
                         f'Запрос не вернул {status.HTTP_201_CREATED}')
        self.assertEqual(count_two - count_one, 1,
                         'Не был добавлен новый клиент')

    def test_unable_create_existing_client(self):
        count_one = Client.objects.count()
        data = {
            'phone': '79263760097',
            'tag': 'man',
            'time_zone': 'Europe/Moscow'
        }
        resp = self.test_client.post(self.api_prefix + 'clients/', data=data)
        count_two = Client.objects.count()
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST,
                         f'Запрос не вернул {status.HTTP_400_BAD_REQUEST}')

        self.assertEqual(resp.json(), {'phone': ['Номер существует']},
                         'Неверный ответ')

        self.assertEqual(count_two, count_one,
                         'Не был добавлен новый клиент')

    def test_unable_create_wrong_number(self):
        count_one = Client.objects.count()
        phones = ['89992221711', '7926376009', '799922233111', '7XXXXXXXXXX']
        data = {
            'tag': 'man',
            'time_zone': 'Europe/Moscow'
        }
        for phone in phones:
            with self.subTest(name=phone):
                data.update({'phone': str(phone)})
                resp = self.test_client.post(self.api_prefix + 'clients/',
                                             data=data)
                self.assertEqual(resp.json(),
                                 {'phone':
                                  ['Неверный номер. '
                                   'Формат номера должен быть  7XXXXXXXXXX']})
                self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
                count_two = Client.objects.count()
                self.assertEqual(count_two, count_one,
                                 'Неверный номер не должен добавляться')
