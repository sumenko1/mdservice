

from django.urls import include, path
from rest_framework import routers

from . import views

app_name = 'storage'

router = routers.DefaultRouter()
router.register(r'mailings', views.MailingViewSet, basename='mailings')
router.register(r'clients', views.ClientViewSet, basename='clients')
router.register(r'messages', views.MessageViewSet, basename='messages')

urlpatterns = [
    path('', include(router.urls)),
]
