from django.contrib import admin
from django.urls import include, path

from .swagger_doc import swagger_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('storage.urls')),
]

urlpatterns += swagger_urlpatterns
