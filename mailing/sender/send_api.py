import os

import requests
from dotenv import load_dotenv


def send_message(id, phone, text):
    """ Отправляет сообщение в удаленный API и возвращает статус """
    status = {'code': 1, 'message': 'Wrong API host', 'status_code': 0}
    if not id or not phone or not text:
        status['message'] = 'Wrong arguments id, phone or text'
        return status

    host = os.environ.get('REMOTE_API_URI')
    token = os.environ.get('REMOTE_API_TOKEN')

    if not host or not token:
        status['message'] = 'Host or token not set. Check environ'
        return status

    headers = {'accept': 'application/json',
               'Authorization': f'Bearer {token}',
               'Content-Type': 'application/json'}
    if host and host[-1] != '/':
        host += '/'
    try:
        json = {'id': int(id), 'phone': int(phone), 'text': str(text)}
    except ValueError as e:
        status['message'] = f'Wrong type "{e}"'
        return status
    response = requests.post(f'{host}{id}', headers=headers, json=json)

    if response:
        json = response.json()
        try:
            if json['code'] == 0 and json['message'] == 'OK':
                return json
        except KeyError:
            status['message'] = 'Status code 200 but wrong API answer'
            return status

    status['status_code'] = response.status_code
    status['message'] = 'Wrong response'
    status['code'] = 2
    return status


if __name__ == '__main__':
    load_dotenv('../.env')

    print('Sending test message')
    s = send_message(1, 123456789, 'Test text')
    print('Return status: ', s)
