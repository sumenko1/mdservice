from datetime import datetime

import pytz
from storage.models import Message

from .send_api import send_message

# from tabulate import tabulate


def send_messages(debug=False):
    """ Отправляет сообщения если пришло время """
    # table = {'phone': [], 'tz': [], 'client time': [],
    #          'start': [], 'stop': [], 'status': [],
    #          'remain_tries': []}

    for message in Message.objects.prefetch_related(
            'mailing', 'client').filter(
            status_send__in=(
            Message.StatusChoices.CREATED,
            Message.StatusChoices.SEND_WAIT)):

        # Сравниваем чистое время без таймзон
        start = message.mailing.start.replace(tzinfo=None)
        stop = message.mailing.stop.replace(tzinfo=None)
        client_time = message.client.client_time.replace(tzinfo=None)

        if start <= client_time <= stop:
            if not debug:
                status = send_message(message.id, message.client.phone,
                                      message.mailing.message)
            else:
                status = {'message': 'OK'}

            if status['message'].upper() == 'OK':
                message.sent_time_utc = datetime.now(pytz.timezone('UTC'))
                message.status_send = Message.StatusChoices.SEND_OK
            elif message.remain_tries > 0:
                message.remain_tries -= 1
            elif message.remain_tries == 0:
                message.status_send = Message.StatusChoices.ERROR
        elif client_time > stop:
            message.status_send = Message.StatusChoices.WRONG_TIME
        elif client_time < start:
            message.status_send = Message.StatusChoices.SEND_WAIT

        # status_text = Message.StatusChoices.choices[message.status_send][1]
        # table['phone'].append(message.client.phone)
        # table['tz'].append(message.client.time_zone)
        # table['client time'].append(client_time)
        # table['start'].append(start)
        # table['stop'].append(stop)
        # table['status'].append(status_text)
        # table['remain_tries'].append(message.remain_tries)

        message.save()

    # print(tabulate(table, headers='keys'))
