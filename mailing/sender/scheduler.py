import os

from apscheduler.schedulers.background import BackgroundScheduler

from .sender import send_messages


def start():
    scheduler = BackgroundScheduler()
    try:
        interval_seconds = int(os.environ.get('INTERVAL_SECONDS', 60))
    except ValueError:
        interval_seconds = 60
    scheduler.add_job(send_messages, 'interval',
                      seconds=interval_seconds)
    scheduler.start()
