from django.apps import AppConfig


class SenderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sender'

    def ready(self):
        # Добавляем слушателя события сохранения рассылки
        # Включаем планировщик
        from . import scheduler, signals

        scheduler.start()
