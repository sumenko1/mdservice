from django.db.models.signals import post_save
from django.dispatch import receiver
from storage.models import Client, Mailing, Message


@receiver(post_save, sender=Mailing)
def new_mailing(sender, **kwargs):
    """ Добавляет сообщения в очередь после создания рассылки """
    mailing = kwargs.get('instance')
    messages = []

    clients = Client.objects.filter(
        mobile_code__in=mailing.filter_mobile_code.all(),
        tag__in=mailing.filter_tags.all())
    messages.extend([Message(mailing=mailing, client=client)
                     for client in clients])
    Message.objects.bulk_create(messages)
